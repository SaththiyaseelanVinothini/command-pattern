package behavioralPattern;

public interface Order {
	 void execute();
}
